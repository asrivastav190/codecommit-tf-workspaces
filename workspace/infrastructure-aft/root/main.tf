module "aft" {
  source = "aws-ia/control_tower_account_factory/aws"
  version = "1.0.12"
  ct_management_account_id    = var.ct_management_account_id
  log_archive_account_id      = var.log_archive_account_id
  audit_account_id            = var.audit_account_id
  aft_management_account_id   = var.aft_management_account_id
  ct_home_region              = var.ct_home_region
  tf_backend_secondary_region = var.tf_backend_secondary_region


  # VCS Vars
  vcs_provider                                  = "bitbucket"
  account_request_repo_name                     = "iso-ne/aft-account-request"
  global_customizations_repo_name               = "iso-ne/aft-global-customizations"
  account_customizations_repo_name              = "iso-ne/aft-account-customizations"
  account_provisioning_customizations_repo_name = "iso-ne/aft-account-provisioning-customizations"

  #TF Vars
  terraform_distribution = "tfc"
  terraform_token        = var.terraform_cloud_token
  terraform_org_name     = "iso-ne"
}