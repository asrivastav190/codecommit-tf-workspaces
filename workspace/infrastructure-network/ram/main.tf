module "create_workload_vpc_ram" {
  providers = {
    aws = aws.use1
  }
  for_each             = toset(var.workload_account_id)
  source               = "git::https://bitbucket.org/asrivastav190/codecommit-tf-modules//ram"
  share_subnets        = var.prv_workload_sn_arn
  workload_account_id  = each.value
  ram_name             = var.prv_workload_vpc_ram_name
  tags                 = var.tags
}

module "create_public_workload_vpc_ram" {
  providers = {
    aws = aws.use1
  }
  for_each             = toset(var.workload_account_id)
  source               = "git::https://bitbucket.org/asrivastav190/codecommit-tf-modules//ram"
  share_subnets        = var.pub_workload_sn_arn
  workload_account_id  = each.value
  ram_name             = var.pub_workload_vpc_ram_name
  tags                 = var.tags
}