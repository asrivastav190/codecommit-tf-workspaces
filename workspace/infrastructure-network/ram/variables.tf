variable "workload_account_id" {
  description = "Provide account id where resources needs to be shared."
  type        = list(string)
  default     = ["672495051411","276304340960"]
}

variable "prv_workload_sn_arn" {
  description = "Provide private workload vpc subnet arn that needs to be shared across AWS accounts."
  type        = list(string)
  default     = ["arn:aws:ec2:us-east-1:419119075743:subnet/subnet-0b279c28e6c68937b","arn:aws:ec2:us-east-1:419119075743:subnet/subnet-00e33651708ea2c46","arn:aws:ec2:us-east-1:419119075743:subnet/subnet-0f69071faaf247702","arn:aws:ec2:us-east-1:419119075743:subnet/subnet-084734094b5b93c99","arn:aws:ec2:us-east-1:419119075743:subnet/subnet-06302620098d20ac4","arn:aws:ec2:us-east-1:419119075743:subnet/subnet-066310ff91c85705f","arn:aws:ec2:us-east-1:419119075743:subnet/subnet-0e9320e1d3679911c","arn:aws:ec2:us-east-1:419119075743:subnet/subnet-0732fd67039995063"]
}

variable "pub_workload_sn_arn" {
  description = "Provide public/private workload vpc subnet arn that needs to be shared across AWS accounts."
  type        = list(string)
  default     = ["arn:aws:ec2:us-east-1:419119075743:subnet/subnet-07593c31f4c9ab340","arn:aws:ec2:us-east-1:419119075743:subnet/subnet-04c20a3996acccce7","arn:aws:ec2:us-east-1:419119075743:subnet/subnet-00829d6981a080015","arn:aws:ec2:us-east-1:419119075743:subnet/subnet-0fe8ec4d7b4e50d90","arn:aws:ec2:us-east-1:419119075743:subnet/subnet-033c817c0ebc7b46d","arn:aws:ec2:us-east-1:419119075743:subnet/subnet-0f087a373894238f8","arn:aws:ec2:us-east-1:419119075743:subnet/subnet-0e0a270bf1fa1c947","arn:aws:ec2:us-east-1:419119075743:subnet/subnet-07f915fc7124b053f"]
  
}

variable "pub_workload_vpc_ram_name" {
  description = "Resource Access Manager Name"
  type        = string
  default     = "pub_workload_vpc_ram_name"
}

variable "prv_workload_vpc_ram_name" {
  description = "Resource Access Manager Name"
  type        = string
  default     = "prv_workload_vpc_ram"
}

## Tags
variable "tags" {
  type    = map(string)
  default = { "iso-environment" = "sandbox", "iso-contact" = "eg@iso-ne.com", "iso-deployment-artifact" = "cmdb", "iso-activity-code" = "1234" }
}