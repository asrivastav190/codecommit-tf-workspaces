
## Tags
variable "tags" {
  type    = map(string)
  default = { "iso-environment" = "sandbox", "iso-contact" = "eg@iso-ne.com", "iso-deployment-artifact" = "cmdb", "iso-activity-code" = "1234" }
}


## Variables declared for private_nacl_sg_rules module.

variable "private_nacl_rules" {
  type = map(map(string))
  default = {
    "ssh_inbound"    = { "from_port" = 22, "to_port" = 22, "protocol" = "tcp", "rule_action" = "deny", "rule_number" = 200, "egress" = false, "cidr_block" = "0.0.0.0/0" },
    "http_inbound"   = { "from_port" = 80, "to_port" = 80, "protocol" = "tcp", "rule_action" = "deny", "rule_number" = 300, "egress" = false, "cidr_block" = "0.0.0.0/0" },
    "https_inbound"  = { "from_port" = 443, "to_port" = 443, "protocol" = "tcp", "rule_action" = "deny", "rule_number" = 400, "egress" = false, "cidr_block" = "0.0.0.0/0" },
    "ssh_outbound"   = { "from_port" = 22, "to_port" = 22, "protocol" = "tcp", "rule_action" = "allow", "rule_number" = 500, "egress" = true, "cidr_block" = "0.0.0.0/0" },
    "http_outbound"  = { "from_port" = 80, "to_port" = 80, "protocol" = "tcp", "rule_action" = "allow", "rule_number" = 600, "egress" = true, "cidr_block" = "0.0.0.0/0" },
    "https_outbound" = { "from_port" = 443, "to_port" = 443, "protocol" = "tcp", "rule_action" = "allow", "rule_number" = 700, "egress" = false, "cidr_block" = "0.0.0.0/0", }
    #  "all_traffic_inbound"  = { "from_port" = 0, "to_port" = 0, "protocol" = "-1", "rule_action" = "allow", "rule_number" = 200, "egress" = false, "cidr_block" = "0.0.0.0/0" },
    #  "all_traffic_outbound" = { "from_port" = 0, "to_port" = 0, "protocol" = "-1", "rule_action" = "allow", "rule_number" = 200, "egress" = true, "cidr_block" = "0.0.0.0/0" }
  }
}
## Security group rules
variable "all_ingress_rules" {
  type = map(map(string))
  default = {
    "all_traffic" = { "type" = "ingress", "from_port" = 0, "to_port" = 0, "protocol" = "-1", "description" = "Allow all ingress", cidr_blocks = "0.0.0.0/0", ipv6_cidr_blocks = "::/0", self = null, security_groups = "", source_security_group_id = null },
    "http"        = { "type" = "ingress", "from_port" = 80, "to_port" = 80, "protocol" = "tcp", "description" = "HTTP from Internet", cidr_blocks = "0.0.0.0/0", ipv6_cidr_blocks = "::/0", self = null, security_groups = "", source_security_group_id = null },
    "http"        = { "type" = "ingress", "from_port" = 443, "to_port" = 443, "protocol" = "tcp", "description" = "HTTPS from Internet", cidr_blocks = "0.0.0.0/0", ipv6_cidr_blocks = "::/0", self = null, security_groups = "", source_security_group_id = null }
  }
}
variable "all_egress_rules" {
  type = map(map(string))
  default = {
    "all_traffic_inbound" = { "type" = "egress", "from_port" = 0, "to_port" = 0, "protocol" = "-1", "description" = "Allow all egress", cidr_blocks = "0.0.0.0/0", ipv6_cidr_blocks = "::/0", self = null, security_groups = "", source_security_group_id = null },
  }
}

variable "vpc_prv_subnets" {
  type = map(object({
    vpc_name           = string
    vpc_id             = string
    private_subnet_ids = list(string)
  }))
  default = {
    pub_prod_workload_vpc = {
      vpc_id             = "vpc-000465c4449e62b45"
      private_subnet_ids = ["subnet-0cef05dd0c7de8e2d","subnet-048c98eb65ec22296","subnet-01e5e3feddb2b5f82","subnet-0e8406d0c905618a8","subnet-0c20f01d9d104c5c1","subnet-0826f102b2e180c05","subnet-0b8099a64e0f03cea","subnet-067ca53db94e11aad"]
      vpc_name           = "public-prod-vpc"
    },
    pub_dev_workload_vpc = {
      vpc_id             = "vpc-052d2561c9f996fce"
      private_subnet_ids = ["subnet-03664d796f5ef4ee7","subnet-06b15a2bd894f08a7","subnet-035969f00e9f61d92","subnet-0dcc27b877a4a84a0","subnet-04b7f5de76efbb90f","subnet-07b9825c16394c2e4","subnet-03cbe5c1d9f2b8f44","subnet-0dced6be8bfd6a762"]
      vpc_name           = "public-dev-vpc"
    }
    prv_prod_workload_vpc = {
      vpc_id             = "vpc-03edd10ea34388027"
      private_subnet_ids = ["subnet-09cd94fbf645eaf4b","subnet-06a9c6d6b8d878d2c","subnet-030539a34dc7ea3d6","subnet-09cbf117a71f013aa"]
      vpc_name           = "production-vpc"
    },
    prv_dev_workload_vpc = {
      vpc_id             = "vpc-0012c44803b1121de"
      private_subnet_ids = ["subnet-0a438368a2c117fa4","subnet-081fc19da0bc80cec","subnet-018570c86621d7971","subnet-06f2094c13eabaa69"]
      vpc_name           = "development-vpc"
    },
    inspection_vpc = {
      vpc_id             = "vpc-0eaeac685181a1c20"
      private_subnet_ids = ["subnet-0b327a5d254725c14","subnet-05765c177985862b8","subnet-0b155b945e824f746","subnet-0603185a554fdc038"]
      vpc_name           = "inspection-vpc"
    },
    ingress_vpc = {
      vpc_id             = "vpc-0843f77f056a16325"
      private_subnet_ids = ["subnet-0688c728c3a0a997e","subnet-0d29279c0ddfe0fcb"]
      vpc_name           = "ingress-vpc"
    }
    egress_vpc = {
      vpc_id             = "vpc-0d4bb0d3c87c816db"
      private_subnet_ids = ["subnet-06dad5ed860386ead","subnet-051c3ee40d1f01d0c"]
      vpc_name           = "egress-vpc"
    }

  }
}

## Variables declared for public_nacl_rules module.
variable "public_nacl_rules" {
  type = map(map(string))
  default = {
    "inbound_traffic"  = { "from_port" = 0, "to_port" = 0, "protocol" = "-1", "rule_action" = "deny", "rule_number" = 100, "egress" = false, "cidr_block" = "0.0.0.0/0" },
    "outbound_traffic" = { "from_port" = 0, "to_port" = 0, "protocol" = "-1", "rule_action" = "allow", "rule_number" = 200, "egress" = true, "cidr_block" = "0.0.0.0/0" },
    #  "all_traffic_inbound"  = { "from_port" = 0, "to_port" = 0, "protocol" = "-1", "rule_action" = "allow", "rule_number" = 100, "egress" = false, "cidr_block" = "0.0.0.0/0" },
    #  "all_traffic_outbound" = { "from_port" = 0, "to_port" = 0, "protocol" = "-1", "rule_action" = "allow", "rule_number" = 200, "egress" = true, "cidr_block" = "0.0.0.0/0" }
  }
}

variable "vpc_pub_subnets" {
  type = map(object({
    vpc_name          = string
    vpc_id            = string
    public_subnet_ids = list(string)
  }))
  default = {
    pub_prod_workload_vpc = {
      vpc_id             = "vpc-000465c4449e62b45"
      public_subnet_ids = ["subnet-033c817c0ebc7b46d","subnet-0f087a373894238f8","subnet-0e0a270bf1fa1c947","subnet-07f915fc7124b053f","subnet-0c87bb7c038b1d35c","subnet-0cda05528e3b576b7","subnet-061021619f5482d5e","subnet-0b3a4e66e28141347"]
      vpc_name           = "public-prod-vpc"
    }
    pub_dev_workload_vpc = {
      vpc_id             = "vpc-052d2561c9f996fce"
      public_subnet_ids = ["subnet-07593c31f4c9ab340","subnet-04c20a3996acccce7","subnet-00829d6981a080015","subnet-0fe8ec4d7b4e50d90","subnet-08d5e425771f14670","subnet-021857038417db623","subnet-07d9dd443c99832f3","subnet-0812b4f5aea54e715"]
      vpc_name           = "public-dev-vpc"
    }
    ingress_vpc = {
      vpc_id            = "vpc-0843f77f056a16325"
      public_subnet_ids = ["subnet-0ef55a57a7fb7d754","subnet-0a12f1627feb42e58"]
      vpc_name          = "ingress-vpc"
    }
    egress_vpc = {
      vpc_id            = "vpc-0d4bb0d3c87c816db"
      public_subnet_ids = ["subnet-03802bb8267c7fc92","subnet-0bc60b9cb861fefd3"]
      vpc_name          = "egress-vpc"
    }

  }
}

# ## Variable declared for inspection_network_firewall module & public_workload_vpc_nfw module.
variable "inspection_vpc_id" {
  default = "vpc-0eaeac685181a1c20"
}

variable "prv_nfw_subnet_ids" {
  default = ["subnet-0b327a5d254725c14","subnet-05765c177985862b8"]
}

variable "prv_tgw_subnet_id" {
  default = "subnet-0b155b945e824f746"
}

variable "pub_workload_vpc_ids" {
  type = map(object({
    vpc_id         = string
    vpc_name       = string
    pub_vpce_rt_id = string
  }))
  default = {
    production_vpc = {
      vpc_id         = "vpc-000465c4449e62b45"
      vpc_name       = "public-prod-vpc"
      pub_vpce_rt_id = "rtb-01c932d233b908339"
    },
    development_vpc = {
      vpc_id         = "vpc-052d2561c9f996fce"
      vpc_name       = "public-dev-vpc"
      pub_vpce_rt_id = "rtb-0181bd4d18947c311"
    }
  }
}

variable "inspection_env" {
  description = "Environment name"
  default     = "InspectionVPC"
}

variable "stateless_rule_group" {
  type        = map(map(string))
  description = "Set of Stateless Rule Groups"
  default = {
    "ephemeral-port-rule" : { "capacity" : 100, "priority" : 1, "actions" : "aws:drop", "source_from_port" : 1024, "source_to_port" : 65535, "protocol" : 6, "source_address" : "0.0.0.0/0", "destination_from_port" : 1024, "destination_to_port" : 65535, "destination_address" : "0.0.0.0/0" },
  }
}

variable "stateful_rule_group" {
  type        = map(map(string))
  description = "Set of Stateful Rule Groups"
  default = {
    "drop-icmp" : { "capacity" : 100, "action" : "DROP", "destination" : "0.0.0.0/0", "destination_port" : 1, "direction" : "ANY", "protocol" : "TCP", "source" : "0.0.0.0/0", "source_port" : 1 },
    "ssh-rule" : { "capacity" : 100, "action" : "DROP", "destination" : "0.0.0.0/0", "destination_port" : 22, "direction" : "ANY", "protocol" : "TCP", "source" : "0.0.0.0/0", "source_port" : 22 },
    "http-rule" : { "capacity" : 100, "action" : "DROP", "destination" : "0.0.0.0/0", "destination_port" : 80, "direction" : "ANY", "protocol" : "TCP", "source" : "0.0.0.0/0", "source_port" : 80 },
    "https-rule" : { "capacity" : 100, "action" : "DROP", "destination" : "0.0.0.0/0", "destination_port" : 443, "direction" : "ANY", "protocol" : "TCP", "source" : "0.0.0.0/0", "source_port" : 443 },
  }
}


variable "inspection_vpc_cidr" {
  description = "Inspection VPC CIDR range"
  default     = "10.161.0.0/16"
}

variable "az" {
  description = "List of Availability Zones to be initialized in Workload VPC"
  type        = set(string)
  default     = ["a", "b","c","d"]
}

variable "pub_sub_suffix" { default = "vpc-pub-workload" }
variable "nfw_sub_suffix" { default = "nfw" }
variable "prv_share_sub_suffix" { default = "vpc-prv-workload" }  