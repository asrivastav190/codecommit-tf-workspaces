variable "region" {
  type = string
  description = "AWS region to deploy resources"
  default = "us-east-1"
}

### Variable definition and declartion for Private Workload VPC. 
### Add VPC/Subnet parameter here in case of new VPC or Subnet requirement.
variable "workload_vpc" {
  type = map(object({
    vpc_name        = string
    cidr_ab         = string
    flow_log_enable = bool
    tgw_subnets = map(object({
      cidr_block = string
      name       = string
      az         = string
    }))
    workload_subnets = map(object({
      cidr_block = string
      name       = string
      az         = string
    }))
  }))
  default = {
    development_vpc = {
      cidr_ab         = "10.165.0.0/16"
      vpc_name        = "development-vpc"
      flow_log_enable = true
      workload_subnets = {
        subnet1 = {
          cidr_block = "10.165.17.0/24"
          name       = "dev-vpc-prv-workload-sn-1"
          az         = "use1-az4"
        }
        subnet2 = {
          cidr_block = "10.165.18.0/24"
          name       = "dev-vpc-prv-workload-sn-2"
          az         = "use1-az6"
        }
        subnet3 = {
          cidr_block = "10.165.19.0/24"
          name       = "dev-vpc-prv-workload-sn-3"
          az         = "use1-az1"
        }
        subnet4 = {
          cidr_block = "10.165.20.0/24"
          name       = "dev-vpc-prv-workload-sn-4"
          az         = "use1-az2"
        }
      }
      tgw_subnets = {
        subnet1 = {
          cidr_block = "10.165.0.112/28"
          name       = "dev-vpc-prv-tgw-sn-1"
          az         = "use1-az4"
        }
        subnet2 = {
          cidr_block = "10.165.0.128/28"
          name       = "dev-vpc-prv-tgw-sn-2"
          az         = "use1-az6"
        }
        subnet3 = {
          cidr_block = "10.165.0.144/28"
          name       = "dev-vpc-prv-tgw-sn-3"
          az         = "use1-az1"
        }
        subnet4 = {
          cidr_block = "10.165.0.160/28"
          name       = "dev-vpc-prv-tgw-sn-4"
          az         = "use1-az2"
        }
      }
    }
    production_vpc = {
      cidr_ab         = "10.166.0.0/16"
      vpc_name        = "production-vpc"
      flow_log_enable = false
      workload_subnets = {
        subnet1 = {
          cidr_block = "10.166.7.0/24"
          name       = "prod-vpc-prv-workload-sn-1"
          az         = "use1-az4"
        }
        subnet2 = {
          cidr_block = "10.166.8.0/24"
          name       = "prod-vpc-prv-workload-sn-2"
          az         = "use1-az6"
        }
        subnet3 = {
          cidr_block = "10.166.9.0/24"
          name       = "prod-vpc-prv-workload-sn-3"
          az         = "use1-az1"
        }
        subnet4 = {
          cidr_block = "10.166.10.0/24"
          name       = "prod-vpc-prv-workload-sn-4"
          az         = "use1-az2"
        }
      }
      tgw_subnets = {
        subnet1 = {
          cidr_block = "10.166.0.112/28"
          name       = "prod-vpc-prv-tgw-sn-1"
          az         = "use1-az4"
        }
        subnet2 = {
          cidr_block = "10.166.0.128/28"
          name       = "prod-vpc-prv-tgw-sn-2"
          az         = "use1-az6"
        }
        subnet3 = {
          cidr_block = "10.166.0.144/28"
          name       = "prod-vpc-prv-tgw-sn-3"
          az         = "use1-az1"
        }
        subnet4 = {
          cidr_block = "10.166.0.160/28"
          name       = "prod-vpc-prv-tgw-sn-4"
          az         = "use1-az2"
        }
      }
    }
  }
}

### This varibale defines parameter for Public Workload VPC ans its subnets

variable "public_workload_vpc" {
  type = map(object({
    vpc_name        = string
    cidr_ab         = string
    flow_log_enable = bool
    tgw_subnets = map(object({
      cidr_block = string
      name       = string
      az         = string
    }))
    nfw_subnets = map(object({
      cidr_block = string
      name       = string
      az         = string
    }))
    private_workload_subnets = map(object({
      cidr_block = string
      name       = string
      az         = string
    }))
    public_workload_subnets = map(object({
      cidr_block = string
      name       = string
      az         = string
    }))
  }))
  default = {
    development_vpc = {
      cidr_ab         = "10.167.0.0/16"
      vpc_name        = "public-dev-vpc"
      flow_log_enable = false
      public_workload_subnets = {
        subnet1 = {
          cidr_block = "10.167.17.0/24"
          name       = "pub-dev-vpc-pub-workload-sn-1"
          az         = "use1-az4"
        }
        subnet2 = {
          cidr_block = "10.167.18.0/24"
          name       = "pub-dev-vpc-pub-workload-sn-2"
          az         = "use1-az6"
        }
        subnet3 = {
          cidr_block = "10.167.19.0/24"
          name       = "pub-dev-vpc-pub-workload-sn-3"
          az         = "use1-az1"
        }
        subnet4 = {
          cidr_block = "10.167.20.0/24"
          name       = "pub-dev-vpc-pub-workload-sn-4"
          az         = "use1-az2"
        }
      }
      tgw_subnets = {
        subnet1 = {
          cidr_block = "10.167.0.112/28"
          name       = "pub-dev-vpc-tgw-sn-1"
          az         = "use1-az4"
        }
        subnet2 = {
          cidr_block = "10.167.0.128/28"
          name       = "pub-dev-vpc-tgw-sn-2"
          az         = "use1-az6"
        }
        subnet3 = {
          cidr_block = "10.167.0.144/28"
          name       = "pub-dev-vpc-tgw-sn-3"
          az         = "use1-az1"
        }
        subnet4 = {
          cidr_block = "10.167.0.160/28"
          name       = "pub-dev-vpc-tgw-sn-4"
          az         = "use1-az2"
        }
      }
       nfw_subnets = {
        subnet1 = {
          cidr_block = "10.167.0.176/28"
          name       = "pub-dev-vpc-nfw-sn-1"
          az         = "use1-az4"
        }
        subnet2 = {
          cidr_block = "10.167.0.192/28"
          name       = "pub-dev-vpc-nfw-sn-2"
          az         = "use1-az6"
        }
        subnet3 = {
          cidr_block = "10.167.0.208/28"
          name       = "pub-dev-vpc-nfw-sn-3"
          az         = "use1-az1"
        }
        subnet4 = {
          cidr_block = "10.167.0.224/28"
          name       = "pub-dev-vpc-nfw-sn-4"
          az         = "use1-az2"
        }
      }
       private_workload_subnets = {
        subnet1 = {
          cidr_block = "10.167.21.0/24"
          name       = "pub-dev-vpc-prv-workload-sn-1"
          az         = "use1-az4"
        }
        subnet2 = {
          cidr_block = "10.167.22.0/24"
          name       = "pub-dev-vpc-prv-workload-sn-2"
          az         = "use1-az6"
        }
        subnet3 = {
          cidr_block = "10.167.23.0/24"
          name       = "pub-dev-vpc-prv-workload-sn-3"
          az         = "use1-az1"
        }
        subnet4 = {
          cidr_block = "10.167.24.0/24"
          name       = "pub-dev-vpc-prv-workload-sn-4"
          az         = "use1-az2"
        }
      }
    }
    production_vpc = {
      cidr_ab         = "10.168.0.0/16"
      vpc_name        = "public-prod-vpc"
      flow_log_enable = false
      public_workload_subnets = {
        subnet1 = {
          cidr_block = "10.168.7.0/24"
          name       = "public-prod-vpc-pub-workload-sn-1"
          az         = "use1-az4"
        }
        subnet2 = {
          cidr_block = "10.168.8.0/24"
          name       = "public-prod-vpc-pub-workload-sn-2"
          az         = "use1-az6"
        }
        subnet3 = {
          cidr_block = "10.168.9.0/24"
          name       = "public-prod-vpc-pub-workload-sn-3"
          az         = "use1-az1"
        }
        subnet4 = {
          cidr_block = "10.168.10.0/24"
          name       = "public-prod-vpc-pub-workload-sn-4"
          az         = "use1-az2"
        }
      }
      tgw_subnets = {
        subnet1 = {
          cidr_block = "10.168.0.112/28"
          name       = "public-prod-vpc-tgw-sn-1"
          az         = "use1-az4"
        }
        subnet2 = {
          cidr_block = "10.168.0.128/28"
          name       = "public-prod-vpc-tgw-sn-2"
          az         = "use1-az6"
        }
        subnet3 = {
          cidr_block = "10.168.0.144/28"
          name       = "public-prod-vpc-tgw-sn-3"
          az         = "use1-az1"
        }
        subnet4 = {
          cidr_block = "10.168.0.160/28"
          name       = "public-prod-vpc-tgw-sn-4"
          az         = "use1-az2"
        }
      }
       nfw_subnets = {
        subnet1 = {
          cidr_block = "10.168.0.176/28"
          name       = "pub-prod-vpc-nfw-sn-1"
          az         = "use1-az4"
        }
        subnet2 = {
          cidr_block = "10.168.0.192/28"
          name       = "pub-prod-vpc-nfw-sn-2"
          az         = "use1-az6"
        }
        subnet3 = {
          cidr_block = "10.168.0.208/28"
          name       = "pub-prod-vpc-nfw-sn-3"
          az         = "use1-az1"
        }
        subnet4 = {
          cidr_block = "10.168.0.224/28"
          name       = "pub-prod-vpc-nfw-sn-4"
          az         = "use1-az2"
        }
      }
       private_workload_subnets = {
        subnet1 = {
          cidr_block = "10.168.21.0/24"
          name       = "pub-prod-vpc-prv-workload-sn-1"
          az         = "use1-az4"
        }
        subnet2 = {
          cidr_block = "10.168.22.0/24"
          name       = "pub-prod-vpc-prv-workload-sn-2"
          az         = "use1-az6"
        }
        subnet3 = {
          cidr_block = "10.168.23.0/24"
          name       = "pub-prod-vpc-prv-workload-sn-3"
          az         = "use1-az1"
        }
        subnet4 = {
          cidr_block = "10.168.24.0/24"
          name       = "pub-prod-vpc-prv-workload-sn-4"
          az         = "use1-az2"
        }
      }
    }
  }
}

# # ## Variables declared for inspection-ingress-egress module

variable "inspection_ingress_egress_param" {
  type = map(object({
    vpc_name        = string
    cidr_ab         = string
    flow_log_enable = bool
  }))
  default = {
    inspection_vpc = {
      cidr_ab         = "10.161.0.0/16"
      vpc_name        = "inspection-vpc"
      flow_log_enable = false
    },
    ingress_vpc = {
      cidr_ab         = "10.162.0.0/16"
      vpc_name        = "ingress-vpc"
      flow_log_enable = false
    },
    egress_vpc = {
      cidr_ab         = "10.163.0.0/16"
      vpc_name        = "egress-vpc"
      flow_log_enable = false
    }
  }
}

variable "az_zone" {
  description = "List of zones to be initialized (subnets, gw etc) in vpc"
  type        = set(string)
  default     = ["a", "b"]
}

variable "az_nums" {
  type = map(string)
  default = {
    a = 1
    b = 2
    c = 3
    d = 4
  }
}

###Transit Gateway variable declaration/definition

variable "transit_gateway_name" {
  type        = string
  description = "Name of Transit Gateway in each region"
  default     = "transit-gateway-use1"
}
variable "peer_transit_gateway_name" {
  type        = string
  description = "Peer transit gateway name in us-west-2 region"
  default     = "peer-transit-gateway-usw2"
}

# ### Variable declared  for s3

variable "app_name" {
  description = "Application Name"
  type        = string
  default     = "iso"
}

variable "app_env" {
  description = "Application Name"
  type        = string
  default     = "dev"
}

variable "aws_region" {
  description = "AWS region"
  default     = "us-east-1"
}

variable "attach_policy" {
  description = "Controls if S3 bucket should have bucket policy attached (set to `true` to use value of `policy` as bucket policy)"
  type        = bool
  default     = true
}

variable "sse_algorithm" {
  description = "The server-side encryption algorithm to use. Valid values are AES256 and aws:kms"
  default     = "AES256"
}

variable "force_destroy" {
  description = "A boolean that indicates all objects should be deleted from the bucket so that the bucket can be destroyed without error"
  default     = true
}

# # https://docs.aws.amazon.com/AmazonS3/latest/dev/acl-overview.html#canned-acl
variable "acl" {
  description = "Set canned ACL on bucket. Valid values are private, public-read, public-read-write, aws-exec-read, authenticated-read, bucket-owner-read, bucket-owner-full-control, log-delivery-write"
  default     = "private"
}


#PUT Bucket acl and PUT Object acl calls will fail if the specified ACL allows public access.
variable "block_public_acls" {
  description = "enable server access log of s3 bucket"
  type        = bool
  default     = true
}
#Ignore public ACLs on this bucket and any objects that it contains.
variable "ignore_public_acls" {
  description = "enable server access log of s3 bucket"
  type        = bool
  default     = true
}

# #Reject calls to PUT Bucket policy if the specified bucket policy allows public access.
variable "block_public_policy" {
  description = "enable server access log of s3 bucket"
  type        = bool
  default     = true
}

# #Only the bucket owner and AWS Services can access this buckets if it has a public policy.
variable "restrict_public_buckets" {
  description = "enable server access log of s3 bucket"
  type        = bool
  default     = true
}

variable "sandbox_account_id" {
  description = "account ID of sandbox account for bucket policy" #need to change account ID and variable name as per requirement
  default     = "419119075743"                                    #eg 419119075743
}

variable "versioning" {
  description = "Map containing versioning configuration." #S3 Versioning to keep multiple versions of an object in one bucket
  type        = bool
  default     = true
}

variable "bucket" {
  description = "Bucket name"
  default     = "iso-s3-bucket"
}

variable "log_bucket" {
  description = "log Bucket name"
  default     = "iso-serveraccesslogs-bucket"
}

variable "replica_log_bucket" {
  description = "replica log Bucket name"
  default     = "replica-iso-serveraccesslogs-bucket"
}

# # ## Variables declared for endpint module

variable "enable_s3_endpoint" {
  description = "Select option to enable/disable s3 endpoint"
  type        = bool
  default     = true
}

## Tags
variable "tags" {
  type    = map(string)
  default = { "iso-environment" = "sandbox", "iso-contact" = "eg@iso-ne.com", "iso-deployment-artifact" = "cmdb", "iso-activity-code" = "1234" }
}

variable "map_public_ip_on_launch" {
  type    = bool
  default = false
}