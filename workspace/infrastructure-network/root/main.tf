### This Module will Create Transit Gateway in us-east-1 and us-west-2 region, route tables and peering attachment

module "create_transit_gateway_infra" {
  providers = {
    aws.tgw     = aws.use1
    aws.peertgw = aws.usw2
  }
  # source                          = "bitbucket.org/iso-ne/codecommit-tf-modules/transit-gateway"
  source                            = "git::https://bitbucket.org/asrivastav190/codecommit-tf-modules//transit-gateway"
  transit_gateway_name              = var.transit_gateway_name
  peer_transit_gateway_name         = var.peer_transit_gateway_name
  central_egress_vpc_attachement_id = module.create_inspection_ingress_egress_vpcs.central_egress_vpc_tgw_attachment_id
  inspection_vpc_tgw_attachement_id = module.create_inspection_ingress_egress_vpcs.inspection_vpc_tgw_attachment_id
  tags                              = var.tags
}


### This Module will create workload vpc based on the parameter values in variable workload_vpc
module "create_workload_vpc" {
  providers = {
    aws = aws.use1
  }
  # source                                = "bitbucket.org/iso-ne/codecommit-tf-modules/workload-vpc"
  source                                  = "git::https://bitbucket.org/asrivastav190/codecommit-tf-modules//workload-vpc"
  for_each                                = var.workload_vpc
  tgw_subnets                             = each.value["tgw_subnets"]
  workload_subnets                        = each.value["workload_subnets"]
  vpc_name                                = each.value["vpc_name"]
  cidr_ab                                 = each.value["cidr_ab"]
  flow_log_enable                         = true
  map_public_ip_on_launch                 = var.map_public_ip_on_launch
  s3_bucket_arn                           = module.create_s3.s3_bucket_arn
  transit_gateway_id                      = module.create_transit_gateway_infra.transit_gateway_id
  spoke_vpc_inspection_tgw_route_table_id = module.create_transit_gateway_infra.spoke_vpc_inspection_tgw_route_table_id
  firewall_subnet_tgw_route_table_id      = module.create_transit_gateway_infra.firewall_subnet_tgw_route_table_id
  tags                                    = var.tags
}


module "create_public_workload_vpc" {
  providers = {
    aws = aws.use1
  }
  # source                                = "bitbucket.org/iso-ne/codecommit-tf-modules/workload-vpc"
  source                                  = "git::https://bitbucket.org/asrivastav190/codecommit-tf-modules//public-workload-vpc-new"
  for_each                                = var.public_workload_vpc
  vpc_name                                = each.value["vpc_name"]
  cidr_ab                                 = each.value["cidr_ab"]
  flow_log_enable                         = each.value["flow_log_enable"]
  tgw_subnets                             = each.value["tgw_subnets"]
  nfw_subnets                             = each.value["nfw_subnets"]
  private_workload_subnets                = each.value["private_workload_subnets"]
  public_workload_subnets                 = each.value["public_workload_subnets"]
  map_public_ip_on_launch                 = var.map_public_ip_on_launch
  s3_bucket_arn                           = module.create_s3.s3_bucket_arn
  transit_gateway_id                      = module.create_transit_gateway_infra.transit_gateway_id
  spoke_vpc_inspection_tgw_route_table_id = module.create_transit_gateway_infra.spoke_vpc_inspection_tgw_route_table_id
  firewall_subnet_tgw_route_table_id      = module.create_transit_gateway_infra.firewall_subnet_tgw_route_table_id
  tags                                    = var.tags
}


### This module will create Inspection/Egress/Ingress vpc based on paremeters value in variable inspection_ingress_egress_param
module "create_inspection_ingress_egress_vpcs" {
  providers = {
    aws = aws.use1
  }
  source                                  = "git::https://bitbucket.org/asrivastav190/codecommit-tf-modules//inspection-ingress-egress-vpc"
  inspection_vpc_param                    = var.inspection_ingress_egress_param["inspection_vpc"]
  ingress_vpc_param                       = var.inspection_ingress_egress_param["ingress_vpc"]
  egress_vpc_param                        = var.inspection_ingress_egress_param["egress_vpc"]
  transit_gateway_id                      = module.create_transit_gateway_infra.transit_gateway_id
  spoke_vpc_inspection_tgw_route_table_id = module.create_transit_gateway_infra.spoke_vpc_inspection_tgw_route_table_id
  firewall_subnet_tgw_route_table_id      = module.create_transit_gateway_infra.firewall_subnet_tgw_route_table_id
  az_zone                                 = var.az_zone
  az_nums                                 = var.az_nums
  s3_bucket_arn                           = module.create_s3.s3_bucket_arn
  region                                  = var.region
  tags                                    = var.tags
}

# # ## S3 MODULE
module "create_s3" {
  # source                           = "bitbucket.org/iso-ne/codecommit-tf-modules/s3"
  source                             = "git::https://bitbucket.org/asrivastav190/codecommit-tf-modules//s3"
  bucket                             = var.bucket
  log_bucket                         = var.log_bucket
  replica_log_bucket                 = var.replica_log_bucket
  aws_region                         = var.aws_region
  acl                                = var.acl
  sse_algorithm                      = var.sse_algorithm
  force_destroy                      = var.force_destroy # make this false later
  attach_policy                      = var.attach_policy # make this true later to attach bucket policy
  s3_policy_rendered                 = data.template_file.s3_bucket_policy.rendered
  app_env                            = var.app_env
  app_name                           = var.app_name
  noncurrent_version_transition_days = 600
  block_public_acls                  = var.block_public_acls
  block_public_policy                = var.block_public_policy
  ignore_public_acls                 = var.ignore_public_acls
  restrict_public_buckets            = var.restrict_public_buckets
  versioning                         = var.versioning
  tags                               = var.tags
}
