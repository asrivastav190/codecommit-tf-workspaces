# # ################ output values for security module

output "public_workload_igw_ingress_rt" {
  value       = zipmap(keys(module.create_public_workload_vpc)[*],values(module.create_public_workload_vpc)[*].igw_ingress_rt)
  description = "Workload VPC route table identifier"
}

output "all_vpc_ids" {
  value = {
    "private_workload_vpcs_id" = zipmap(keys(module.create_workload_vpc)[*], values(module.create_workload_vpc)[*].vpc_id)
    "public_workload_vpcs_id" = zipmap(keys(module.create_public_workload_vpc)[*],values(module.create_public_workload_vpc)[*].vpc_id)
	  "inspection_vpc_id" = module.create_inspection_ingress_egress_vpcs.inspection_vpc_id
	  "ingress_vpc_id" = module.create_inspection_ingress_egress_vpcs.ingress_vpc_id
	  "egress_vpc_id" = module.create_inspection_ingress_egress_vpcs.egress_vpc_id
  }
}

output "all_vpc_private_subnet_ids" {
    value = {
    "private_workload_vpc" = zipmap(keys(module.create_workload_vpc)[*],values(module.create_workload_vpc)[*].tgw_subnet_ids)
    "public_workload_vpc_tgw" = zipmap(keys(module.create_public_workload_vpc)[*],values(module.create_public_workload_vpc)[*].workload_vpc_tgw_subnet)
    "public_workload_vpc_prv_sn"= zipmap(keys(module.create_public_workload_vpc)[*],values(module.create_public_workload_vpc)[*].workload_vpc_private_workload_subnet)
	  "inspection_vpc_tgw" = module.create_inspection_ingress_egress_vpcs.prv_tgw_subnet_ids
    "inspection_vpc_nfw" = module.create_inspection_ingress_egress_vpcs.prv_nfw_subnet_ids
	  "ingress_vpc_tgw" = module.create_inspection_ingress_egress_vpcs.ingress_prv_subnet_ids
	  "egress_vpc_tgw" = module.create_inspection_ingress_egress_vpcs.egress_prv_subnet_ids
  }
}

output "vpcs_public_subnet_ids" {
    value = {
	    "public_workload_pub_sn" = zipmap(keys(module.create_public_workload_vpc)[*],values(module.create_public_workload_vpc)[*].workload_vpc_public_workload_subnet)
      "public_workload_vpc_nfw" = zipmap(keys(module.create_public_workload_vpc)[*],values(module.create_public_workload_vpc)[*].workload_vpc_nfw_subnet)
	    "ingress_vpc_pub_sn" = module.create_inspection_ingress_egress_vpcs.ingress_pub_subnet_ids
	    "egress_vpc_pub_sn" = module.create_inspection_ingress_egress_vpcs.egress_pub_subnet_ids
  }
}

## output for ram module
output "private_workload_vpc_sn_arn" {
  value = zipmap(keys(module.create_workload_vpc)[*],values(module.create_workload_vpc)[*].subnet_arn)
}

output "public_workload_vpc_prv_sn_arn" {
  value = zipmap(keys(module.create_public_workload_vpc)[*],values(module.create_public_workload_vpc)[*].workload_vpc_private_workload_subnet_arn)
}

output "public_workload_pub_sn_arn" {
  value = zipmap(keys(module.create_public_workload_vpc)[*],values(module.create_public_workload_vpc)[*].workload_vpc_public_workload_subnet_arn)
}


