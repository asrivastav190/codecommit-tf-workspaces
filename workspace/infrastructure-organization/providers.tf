terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.73.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
  alias  = "use1"
}

provider "aws" {
  alias  = "pwc_explore"
  region = "us-east-1"
  profile = "pwc_explore"
  assume_role {
    role_arn     = var.role
  }
}