variable "role_arn" {}
# ## Variables deaclared for ou_policies module
#TAG
variable "tag_policy_name" {
  type    = string
  default = "Tag_policy_0101"
}

variable "name" {
  type        = string
  description = "Tag name can contain a-z,0-9,-."
  default     = "anki-123"
  validation {
    condition     = can(regex("\\b\\w*[-]\\w*\\b", var.name))
    error_message = "Tag name can contain a-z,0-9,-."
  }
}

variable "iso_deployment_artifact_value" {
  type        = string
  description = "Must have cmdb- followed by number. For eg: cmdb-1234"
  default     = "cmdb-1234"
  validation {
    condition     = can(regex("(?:cmdb-)+\\d", var.iso_deployment_artifact_value))
    error_message = "Must have cmdb- followed by number."
  }
}

variable "iso_environment_value" {
  type        = string
  description = "Options: development,integration,pre-production,production,sandbox and user."
  default     = "sandbox"

  validation {
    condition     = contains(["development", "integration", "pre-production", "production", "sandbox", "user"], var.iso_environment_value)
    error_message = "Environment must be development,integration,pre-production,production,sandbox and user ."
  }
}

variable "iso_activity_code" {
  type        = string
  description = "Must have positive integer eg-1234"
  default     = "1234"
  validation {
    condition     = can(regex("^\\+?[1-9]$|^\\+?\\d+$", var.iso_activity_code))
    error_message = "Must have positive integer eg-1234."
  }
}

variable "iso_contact_value" {
  type      = string
  description = "Must have email in format eg-12@iso-ne.com. For eg: iso-12@iso-ne.com"
  default     = "iso-12@iso-ne.com"
  validation {
    condition     = can(regex("[a-zA-Z]+[-]+[0-9]+@(?:iso-ne.com)+$", var.iso_contact_value))
    error_message = "Must have email in format alpha-num@iso-ne.com."
  }
  
}

#SCP
variable "group_name" {
  type    = string
  default = "Full-Access"
}

variable "sso_perms_name" {
  type    = string
  default = "allowed_marketplace_0101"
}

variable "marketplace_rule_name" {
  type    = string
  default = "Deny Marketplace Access 0101"
}

variable "tags" {
  type    = map(string)
  default = { "iso-environment" = "sandbox", "iso-contact" = "eg@iso-ne.com", "iso-deployment-artifact" = "cmdb","iso-activity-code" = "1234" }
}

variable "sandbox_account_id" {
  description = "account ID of sandbox account for bucket policy" #need to change account ID and variable name as per requirement
  default     = "419119075743"                                    #eg 419119075743
}