# Configure Guardduty resources in the audit account
module "guardduty" {
    providers = {
        aws = aws.audit-acct
    }

    source  = "app.terraform.io/iso-ne/guardduty/isone"
    version = "0.1.0"
    sso_adminaccess_arn = var.sso_adminaccess_arn
    audit_account = var.audit_account
    log_archive_account = var.log_archive_account
}

# Configure S3 buckets and permissions for security
module "logging" {
    providers = {
        aws.east = aws.log-archive-acct-east
        aws.west = aws.log-archive-acct-west
    }
    source  = "app.terraform.io/iso-ne/logging/isone"
    version = "0.1.0"
    guardduty_s3_encryption_key_arn = module.guardduty.guardduty_s3_encryption_key_arn
}

# Configure roles and policies to allow Prisma Cloud access.  We 
# need to add several accounts here due to the fact the AFT
# does not manage all of the core accounts.  We need to provision
# access in the root, aws-aft-management, audit, and log-archive
# accounts

# Configure root account
module "prismacloud" {
    providers = {
        aws = aws.control-tower-acct
    }
    source = "app.terraform.io/iso-ne/prismacloud/isone"
    version = "0.1.6"
    prismacloud-external-id = var.prismacloud_external_id
}

# Configure aws-aft-management account
module "prismacloud-aft-acct" {
    providers = {
        aws = aws.aws-aft-management-acct
    }
    source = "app.terraform.io/iso-ne/prismacloud/isone"
    version = "0.1.6"
    prismacloud-external-id = var.prismacloud_member_external_id
    prismacloud-role-name = "PrismaCloudOrgReadOnlyRole"
}

# Configure audit account
module "prismacloud-audit-acct" {
    providers = {
        aws = aws.audit-acct
    }
    source = "app.terraform.io/iso-ne/prismacloud/isone"
    version = "0.1.6"
    prismacloud-external-id = var.prismacloud_member_external_id
    prismacloud-role-name = "PrismaCloudOrgReadOnlyRole"
}

# Configure log-archive acct
module "prismacloud-log-archive-acct" {
    providers = {
        aws = aws.log-archive-acct-east
    }
    source = "app.terraform.io/iso-ne/prismacloud/isone"
    version = "0.1.6"
    prismacloud-external-id = var.prismacloud_member_external_id
    prismacloud-role-name = "PrismaCloudOrgReadOnlyRole"
}