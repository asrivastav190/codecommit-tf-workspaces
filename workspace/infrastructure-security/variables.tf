variable "sso_adminaccess_arn" {
    description = "The ARN for the SSO Administrator Role"
    type = string
}

variable "audit_account" {
    description = "The account ID for the Control Tower audit account"
    type = string
}

variable "log_archive_account" {
    description = "The account ID for the Control Tower log_archive account"
    type = string
}

variable "prismacloud_external_id" {
    description = "The external ID for Prismacloud to assume the required STS role for monitoring the org"
    type = string
}

variable "prismacloud_member_external_id" {
    description = "The external ID for Prismacloud to assume the required STS role for monitoring an organization member acccount"
    type = string
}