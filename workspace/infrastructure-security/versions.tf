terraform {
  backend "remote" {
    organization = "iso-ne"
    workspaces {
      name = "aws-infrastructure-security"
    }
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.70.0"
    }
  }

  required_version = ">= 0.14.0"
}