provider "aws" {
  region = "us-east-1"
}

provider "aws" {
  alias = "control-tower-acct"  
  region = "us-east-1"
  
  assume_role {
    role_arn    = "arn:aws:iam::167468505245:role/iso-terraform-access"
    external_id = "iso-terraform-cloud"
  }

  default_tags {
    tags = {
      iso-environment = "production"
      iso-deployment-artifact = "tbd"
      iso-contact = "aws-root@iso-ne.com"
      iso-activity-code = "712036"
    }
  }
}

provider "aws" {
  alias = "audit-acct"  
  region = "us-east-1"

  default_tags {
    tags = {
      iso-environment = "production"
      iso-deployment-artifact = "tbd"
      iso-contact = "aws-root@iso-ne.com"
      iso-activity-code = "712036"
    }
  }

  assume_role {
    role_arn    = "arn:aws:iam::571208567886:role/iso-terraform-access"
    external_id = "iso-terraform-cloud"
  }
}

provider "aws" {
  alias = "log-archive-acct-east"
  region = "us-east-1"

  default_tags {
    tags = {
      iso-environment = "production"
      iso-deployment-artifact = "tbd"
      iso-contact = "aws-root@iso-ne.com"
      iso-activity-code = "712036"
    }
  }
  
  assume_role {
    role_arn    = "arn:aws:iam::938251343003:role/iso-terraform-access"
    external_id = "iso-terraform-cloud"
  }
}

provider "aws" {
  default_tags {
    tags = {
      iso-environment = "production"
      iso-deployment-artifact = "tbd"
      iso-contact = "aws-root@iso-ne.com"
      iso-activity-code = "712036"
    }
  }

  alias = "log-archive-acct-west"
  region = "us-west-2"

  assume_role {
    role_arn = "arn:aws:iam::938251343003:role/iso-terraform-access"
    external_id = "iso-terraform-cloud"
  }
}

provider "aws" {
  alias = "aws-aft-management-acct"
  region = "us-east-1"

  assume_role {
    role_arn = "arn:aws:iam::390018995663:role/iso-terraform-access"
    external_id = "iso-terraform-cloud"
  }

  default_tags {
    tags = {
      iso-environment = "production"
      iso-deployment-artifact = "tbd"
      iso-contact = "aws-root@iso-ne.com"
      iso-activity-code = "712036"
    }
  }
}